"use strict"

/*

Теоретичні питання
1. Які способи JavaScript можна використовувати для створення та додавання нових DOM-елементів?

Є кілька способів використання JavaScript для створення та додавання нових DOM-елементів:

1) Створення елементу:
Використовуйте конструктор document.createElement(tagName) для створення нового DOM-елементу з вказаним ім'ям тегу. Наприклад, щоб створити новий елемент <div>:
let newDiv = document.createElement('div');

2) Додавання текстового вмісту:
Використовуйте властивість textContent або innerText для додавання текстового вмісту в новий елемент. Наприклад:
newDiv.textContent = 'Це новий елемент!';

3) Додавання атрибутів:
Використовуйте метод setAttribute для додавання атрибутів новому елементу. Наприклад:
newDiv.setAttribute('class', 'новий-клас');

4) Додавання елементу до DOM:
Використовуйте методи для додавання створених елементів до DOM-структури. Наприклад, для додавання елементу до іншого елементу використовуйте метод appendChild:
let parentElement = document.getElementById('parent');
parentElement.appendChild(newDiv);

5) Вставка перед/після іншого елемента:
Використовуйте методи insertBefore або власні функції для вставки елементів перед або після іншого елемента. Приклад:
let referenceElement = document.getElementById('reference');
parentElement.insertBefore(newDiv, referenceElement);

6) Використання innerHTML:
Використовуйте властивість innerHTML для вставки HTML-коду в елемент. Цей метод корисний, але потрібно бути обережним з введенням користувача, щоб уникнути вразливостей щодо впровадження скриптів (XSS). Приклад:
parentElement.innerHTML = '<p>Новий HTML-код</p>';

Обирайте метод, який найбільше підходить для вашого випадку в залежності від конкретних вимог та сценаріїв.

2. Опишіть покроково процес видалення одного елементу (умовно клас "navigation") зі сторінки.
Для видалення одного елементу зі сторінки за допомогою JavaScript слід виконати наступні кроки:

1)Отримання посилання на елемент:
Використати метод document.querySelector або document.getElementsByClassName для отримання посилання на елемент за його класом "navigation". Наприклад:
let navigationElement = document.querySelector('.navigation');
2) Перевірка існування елементу:

Перевірити, чи знайдений елемент існує перед продовженням. Це може бути важливим у випадку, коли елемент може відсутній на сторінці. Наприклад:
if (navigationElement) {
    // Продовжуйте з видаленням елементу
} else {
    console.log('Елемент з класом "navigation" не знайдено.');
}
3) Видалення елементу:
Використати метод remove для видалення елементу з DOM. Цей метод не підтримується Internet Explorer, тому може бути важливо використовувати альтернативи для більшої сумісності. Приклад:
if (navigationElement) {
    navigationElement.remove(); // Видалення елементу
}
Якщо треба підтримувати Internet Explorer, можна використати наступний код:
if (navigationElement && navigationElement.parentNode) {
    navigationElement.parentNode.removeChild(navigationElement);
}
Альтернативно, треба використати бібліотеку, таку як jQuery, яка забезпечує кросс-браузерність для подібних операцій:
$('.navigation').remove();
Ці кроки дозволять вам безпечно видалити елемент зі сторінки за допомогою JavaScript.

3. Які є методи для вставки DOM-елементів перед/після іншого DOM-елемента?
Для вставки DOM-елементів перед або після іншого DOM-елемента використовуються наступні методи:

1)Вставка перед елементом:
insertBefore(newNode, referenceNode): Цей метод вставляє новий вузол newNode перед існуючим вузлом referenceNode. Він викликається на батьківському елементі.
Приклад:
const parentElement = document.getElementById('parent');
const newElement = document.createElement('div');
parentElement.insertBefore(newElement, referenceElement);

2) Вставка після елемента:
insertAfter(newNode, referenceNode): У ряді браузерів немає вбудованого методу insertAfter. Проте, можна використати наступний код:
Приклад:
function insertAfter(newNode, referenceNode) {
    referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
}

const parentElement = document.getElementById('parent');
const newElement = document.createElement('div');
const referenceElement = document.getElementById('reference');
insertAfter(newElement, referenceElement);
Обидва ці методи можуть бути використані для динамічного створення і вставки нових DOM-елементів у визначене місце в структурі сторінки.
*/

/*
Практичні завдання
1. Створіть новий елемент <a>, задайте йому текст "Learn More" і атрибут href з посиланням на "#". Додайте цей елемент в footer після параграфу.
*/

const newLink = document.createElement('a');
newLink.textContent = 'Learn More';
newLink.setAttribute('href', '#');

const footer = document.querySelector('footer');
const paragraph = footer.querySelector('p');
footer.insertBefore(newLink, paragraph.nextSibling);


/*
2. Створіть новий елемент <select>. Задайте йому ідентифікатор "rating", і додайте його в тег main перед секцією "Features".
Створіть новий елемент <option> зі значенням "4" і текстом "4 Stars", і додайте його до списку вибору рейтингу.
Створіть новий елемент <option> зі значенням "3" і текстом "3 Stars", і додайте його до списку вибору рейтингу.
Створіть новий елемент <option> зі значенням "2" і текстом "2 Stars", і додайте його до списку вибору рейтингу.
Створіть новий елемент <option> зі значенням "1" і текстом "1 Star", і додайте його до списку вибору рейтингу.
*/

const selectElem = document.createElement('select');
selectElem.setAttribute('id', 'rating');

const optionsData = [
    {value: '4', text: '4 Stars'},
    {value: '3', text: '3 Stars'},
    {value: '2', text: '2 Stars'},
    {value: '1', text: '1 Star'},
];

optionsData.forEach(data => {
    const optionElem = document.createElement('option');
    optionElem.value = data.value;
    optionElem.textContent = data.text;
    selectElem.appendChild(optionElem);
});


const mainElement = document.querySelector('main');
const featuresSection = document.getElementById('features');

mainElement.insertBefore(selectElem, featuresSection);